// Requires
const opciones = {
    base: {
        demand: true,
        alias: 'b',
        // default: 10,
        description: 'Num. Base de la tabla de multiplicar a crear.'
    },

    limite: {
        // demand: false,
        alias: 'l',
        default: 10,
        description: 'Num. Opcional. Límite máximo de multiplicaciones.'
    }
}

// MAIN
const argv = require('yargs')
                .command('crear', ': Crea la tabla de multiplicar.', opciones)
                .command('listar', ': Imprime en consla la tabla de multiplicar.', opciones)
                .help()
                .argv;

module.exports = {
    argv
};