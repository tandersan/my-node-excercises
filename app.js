// Requires
const { crearArchivo, listarTabla } = require('./fx/multiplicar');
const { argv } = require('./config/yargs');
const colores = require('colors');

// Variables
let mainMenu = argv._[0];
let base = argv.base;
let limite = argv.limite;

// MAIN
switch(mainMenu) {
    case 'crear':
        crearArchivo(base, limite)
            .then ( nombreArchivo => console.log(`Archivo ${nombreArchivo} para base ${base} creado exitosamente.`.green) )
            .catch( err => console.log(err.red));
        break;

    case 'listar':  
        listarTabla(base, limite)
            .then ( data => console.log(`${data}`.green) )
            .catch( err => console.log(err));
        break;

    default:
        console.log('[Comando no reconocido : '.inverse + mainMenu.bgWhite.red + ' ]'.inverse);
        break;
}