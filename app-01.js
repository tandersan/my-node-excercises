// Requires
const { crearArchivo } = require('./codigo/multiplicar');

// Variables
//let base = 2;
//let base = '1'; // La función de node que evalúa el número, acepta '1'.
//let base = 'asd'; // Genera error.
if(typeof process.argv[2] == 'undefined')
    base = 1;
else
    base = process.argv[2].split('=')[1]; //Obtenemos el Tercer Argumento. Separamos por el =, y así obtenemos el número.


// console.log(process);
// console.log(process.argv);

crearArchivo(base)
    .then ( nombreArchivo => console.log(`Archivo ${nombreArchivo} para base ${base} creado exitosamente.`) )
    .catch( err => console.log(err));