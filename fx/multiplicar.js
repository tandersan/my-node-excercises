// Requires
const fs = require('fs');

// Variables
let dirBase = 'tablas';
let data = '';

// MAIN
let crearArchivo = ( (base, limite) => {
    let nombreArchivo = `tabla-${base}.txt`;

    return new Promise ( (resolve, reject) => {
        if (!Number(base)) {
            reject(`La base [${base}] utilizada no es un número válido.`);
            return;
        }

        for (let contador=1; contador<=limite; contador++) {
            data += `${base} * ${contador} = ${base*contador}\n`;
        }
        
        fs.writeFile(`./${dirBase}/${nombreArchivo}`, data, (err) => {
            if (err)
                reject(err)
            else
                resolve(nombreArchivo);
        });
    });
});

let listarTabla = ( (base, limite) => {
    return new Promise ( (resolve, reject) => {
        if (!Number(base)) {
            reject(`La base [${base}] utilizada no es un número válido.`);
            return;
        }

        for (let contador=1; contador<=limite; contador++) {
            data += `${base} * ${contador} = ${base*contador}\n`;
        }

        resolve(data);
    });
});

module.exports = {
    crearArchivo,
    listarTabla
};